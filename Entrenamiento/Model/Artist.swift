//
//  Artist.swift
//  Entrenamiento
//
//  Created by Prats on 16/06/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import Foundation
import ObjectMapper

class Artist: Mappable {
    var name: String!
    var genres: [String]!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name    <- map["name"]
        genres  <- map["genres"]
    }
}

class Album: Mappable {
    var type: String!
    var images: [Image]!
    var name: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        type    <- map["album_type"]
        images  <- map["images"]
        name    <- map["name"]
    }
}

class Image: Mappable {
    var url: String!
    var heigth: Int!
    var width: Int!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        url     <- map["url"]
        heigth  <- map["height"]
        width   <- map["width"]
    }
    
    
}
