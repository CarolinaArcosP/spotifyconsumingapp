//
//  User.swift
//  Entrenamiento
//
//  Created by tatiana arcos prats on 14/03/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import Foundation
import OAuthSwift

class User {
    let name: String
    let email: String
    
    init(name: String, email: String) {
        self.name = name
        self.email = email
    }
    
    static func login() {
       let oauth = OAuth2Swift(consumerKey: CLIENT_ID,
                               consumerSecret: CLIENT_SECRET,
                               authorizeUrl: "https://accounts.spotify.com/authorize",
                               responseType: "code")
        let _ = oauth.authorize(withCallbackURL: REDIRECT_URI,
                                     scope: "",
                                     state: "SpotifyConsumingApp",
                                     success: { (credential, response, parameters) in
                                        print(credential.oauthToken)
                                        
        }, failure: {
            error in
            print(error.localizedDescription)
        })
    }
}
