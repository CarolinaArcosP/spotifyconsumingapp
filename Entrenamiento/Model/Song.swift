//
//  Song.swift
//  Entrenamiento
//
//  Created by Prats on 10/06/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class Song: Mappable {
    var name: String!
    var album: String! //TODO: add album model
    var artists: [Artist]!
    var popularity: Int!
    var preview_url: String!
    
    var artistsString: String {
        return self.artists.flatMap({ (artist) in
            artist.name
        }).joined(separator: ", ")
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name        <- map["name"]
        album       <- map["album"]
        artists     <- map["artists"]
        popularity  <- map["popularity"]
        preview_url <- map["preview_url"]
    }
    
//    class func getSongs(of type: String, matching: String, response: @escaping (_ song: [Song]?, _ error: String?) -> Void) {
//        provider.request(Router.search(type: "track", keyword: "some")) { (result) in
//            switch result {
//            case let .success(moyaResponse):
//                do {
//                    try moyaResponse.filterSuccessfulStatusCodes()
//                    let a = try moyaResponse.mapJSON()
//                    let dictionary = try moyaResponse.mapJSON() as? [[String : Any]]
//                    var songs = [Song]()
//                    for item in dictionary! {
//                        guard let song = Song(JSON: item) else { continue }
//                        songs.append(song)
//                    }
//
//                    response(songs, nil)
//                } catch {
//                    response(nil, error.localizedDescription)
//                }
//            case let .failure(error):
//                response(nil, error.localizedDescription)
//                break
//            }
//        }
//    }
    
    class func getSongs(of type: String, matching text: String, completion: @escaping (_ song: [Song]?, _ error: String?) -> Void) {
        guard let url = URL(string: "https://api.spotify.com/v1/search") else {
            completion(nil, "Invalid URL")
            return
        }
        let queue = DispatchQueue.global()
        Alamofire.request(url,
                          method: .get,
                          parameters: ["q" : text,
                                       "type" : type],
                          encoding: URLEncoding.default,
                          headers: ["Content-Type" : "application/json",
                                    "Authorization" : "Bearer \(globalKeychain.search(key: "access_token") ?? "")"])
            .responseJSON(queue: queue) { response in
                switch response.result {
                case .success:
                    guard let json = response.result.value else {
                        print("ERROR!!")
                        completion(nil, "Error :(")
                        return
                    }
                    
                    guard let data = json as? [String: Any] else {
                        return
                    }
                    
                    guard let tracks = data["tracks"] as? [String : Any] else {
                        return
                    }
                    
                    guard let items = tracks["items"] as? [[String : Any]] else {
                        return
                    }
                    
                    let songs = items.flatMap({ (item) in
                        return Song(JSON: item)
                    })
                    
                    completion(songs, nil)
                case .failure(let error):
                    completion(nil, error.localizedDescription)
                }
        }
    }
}
