//
//  SongDetailViewController.swift
//  Entrenamiento
//
//  Created by tatiana arcos prats on 14/03/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import UIKit

class SongDetailViewController: UIViewController {

    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var ratingValueLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    var movieDetails: Song?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDetailView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper methods
    
    func configureDetailView() {
        if let movieDetails = movieDetails {
            movieNameLabel.text = movieDetails.name
            ratingValueLabel.text = "\(movieDetails.popularity ?? 0)"
            artistLabel.text = movieDetails.artistsString
        }
    }

}
