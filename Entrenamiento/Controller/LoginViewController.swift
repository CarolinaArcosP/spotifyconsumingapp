//
//  LoginViewController.swift
//  Entrenamiento
//
//  Created by tatiana arcos prats on 14/03/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import UIKit
import OAuthSwift

class LoginViewController: UIViewController {
    var oauthSwift: OAuth2Swift?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func login(_ sender: UIButton) {
        let oauth = OAuth2Swift(consumerKey: CLIENT_ID,
                                consumerSecret: CLIENT_SECRET,
                                authorizeUrl: AUTHORIZE_URL,
                                accessTokenUrl: ACCESS_TOKEN_URL,
                                responseType: "code")
        
        self.oauthSwift = oauth
        oauth.authorizeURLHandler = SafariURLHandler(viewController: self, oauthSwift: oauthSwift!)
        
        let _ = oauth.authorize(withCallbackURL: URL(string: REDIRECT_URI)!,
                        scope: "user-read-private",
                        state: "34fFs29kd09",
                        success: { (credential, response, parameters) in
                            if let params = parameters as? [String : Any] {
                                print("Parameters: ", params)
                                let keychain = Keychain()
                                keychain.update(value: params["access_token"] as! String)
//                                keychain.update(value: params["refresh_token"] as! String)
                            }
                            
                            self.dismiss(animated: true, completion: nil)
        }, failure: { error in
            print(error.localizedDescription)
        })
        
    }

}
