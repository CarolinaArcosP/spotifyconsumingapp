//
//  MoviesViewController.swift
//  Entrenamiento
//
//  Created by tatiana arcos prats on 14/03/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    // MARK: - Constants
    struct Constants {
        static let segueDetails = "showSongDetail"
        static let trackType = "track"
        static let movideCellIdentifier = "movieCell"
    }
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var songs = [Song]()
    
    // MARK: - Initializer
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Helper methods
    
    func getSongs(of track: String, matching text: String) {
        SongViewModel.getSharedInstance().getSongs(of: track, matching: text) { (songs, error) in
            guard let songs = songs else {
                guard let error = error else {
                    print("Unkown error")
                    return
                }
                print("Error: \(error)")
                return
            }
            
            DispatchQueue.main.async {
                self.songs = songs
                self.tableView.reloadData()
            }
            
        }
    }
    
    func configureCell(for cell: SongCell, with song: Song) {
        cell.movieNameLabel.text = song.name
        cell.artistLabel.text = song.artistsString
        cell.rateLabel.text = String(song.popularity)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueDetails {
            let controller = segue.destination as! SongDetailViewController
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                controller.movieDetails = songs[indexPath.row]
            }
        }
    }
}

extension ResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.movideCellIdentifier, for: indexPath) as? SongCell else {
            let alert = UIAlertController(title: "Error", message: "Try again :(", preferredStyle: .alert)
            self.show(alert, sender: self)
            return UITableViewCell()
        }
        
        configureCell(for: cell, with: songs[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ResultsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        becomeFirstResponder()
        getSongs(of: Constants.trackType, matching: text)
    }
}



