//
//  Keychain.swift
//  Entrenamiento
//
//  Created by Prats on 10/06/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import Foundation
import Security

struct Keychain {
    enum KeychainError: Error {
        case noItem
        case unexpectedItemData
        case keyNotFound
        case unhandledError(status: OSStatus)
    }
    
    let tag: Data
    
    init(tag: String? = "com.carolina.Entrenamiento") {
        self.tag = (tag!.data(using: .utf8)!)
    }
    
    func generateQuery() -> NSMutableDictionary {
        let keychainQuery = NSMutableDictionary(dictionary:
            [kSecClass as String: kSecClassKey,
             kSecAttrApplicationTag as String: tag])
        
        return keychainQuery
    }
    
    func save(service: String, value: String) {
        do {
            let dataFromString: Data = value.data(using: String.Encoding.utf8)!
            
            let keychainQuery = generateQuery()
            keychainQuery[kSecValueData as String] = dataFromString
            
            //Add item to keychain
            let status = SecItemAdd(keychainQuery as CFDictionary, nil)
            
            guard status == errSecSuccess else {
                throw KeychainError.unhandledError(status: status)
            }
            
            print("saved")
        } catch {
            print("Error: \(error.localizedDescription)")
            //TODO: add error handling
        }
    }
    
    func update(value: String) {
        do {
            let dataFromString: Data = value.data(using: String.Encoding.utf8)!
            
            let keychainQuery = generateQuery()
            let attributes = [kSecValueData as String: dataFromString]
            
            let status = SecItemUpdate(keychainQuery as CFDictionary, attributes as CFDictionary)
            
            guard status != errSecItemNotFound else {
                self.save(service: "", value: value)
                return
            }
            
            guard status == errSecSuccess else {
                throw KeychainError.unhandledError(status: status)
            }
            
            print("Updated")
        } catch {
            print("Error: \(error.localizedDescription)")
        }

        
    }
    
    func search(key: String) -> String? {
        do {
            let keychainQuery = generateQuery()
            keychainQuery[kSecMatchLimit as String] = kSecMatchLimitOne
            keychainQuery[kSecReturnData as String] = true
            
            
            var itemRefType: AnyObject?
            
            let status = SecItemCopyMatching(keychainQuery, &itemRefType)
            
            guard status != errSecItemNotFound else {
                throw KeychainError.noItem
            }
            
            guard status == errSecSuccess else {
                throw KeychainError.unhandledError(status: status)
            }
            
            guard let data = itemRefType as? Data else {
                throw KeychainError.unexpectedItemData
            }
            
            let value = String(data: data, encoding: String.Encoding.utf8)
            return value
            
//            guard let data = itemRefType as? [[String: Any]] else {
//                throw KeychainError.unexpectedItemData
//            }
//
//            guard let pairValue = data.filter({ (pair) -> Bool in
//                pair.index(forKey: key) != nil
//            }).first else {
//                throw KeychainError.keyNotFound
//            }
//
//            return (pairValue[key] as! String)
        } catch {
            return nil
        }
    }
}
