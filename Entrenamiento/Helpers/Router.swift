//
//  Router.swift
//  Entrenamiento
//
//  Created by Prats on 22/05/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import Foundation
import Moya

let globalKeychain = Keychain()

enum Router {
    case login(username: String, password: String)
    case search(type: String, keyword: String)
}

let provider = MoyaProvider<Router>()

extension Router: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://accounts.spotify.com")!
    }
    
    var path: String {
        switch self {
        case .login(let username, let password):
            return "/authorize"
        case .search(type: _, keyword: _):
            return "v1/search?"
        }
    }
    
    //TODO: Modify parameters
    var parameters: [String : Any] {
        switch self {
        case .login(let username, let password):
            return ["client_id" : CLIENT_ID,
                    "response_type": "code",
                    "redirect_uri" : REDIRECT_URI]
        case .search(let type, let keyword):
            return ["q" : keyword,
                    "type" : type]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var method: Moya.Method {
        switch self {
        case .login(_,_):
            return .get
        case .search(_, _):
            return .get
        }
    }
    
    //TODO: modify sampleData
    var sampleData: Data {
        switch self {
        case .login(_,_):
            return "{\"code\": NApCCg..BkWtQ, \"status\": profile%2Factivity".data(using: .utf8)!
        case .search(_,_):
            return "".data(using: .utf8)!
        }
    }
    
    var task: Task {
        switch self {
        case .login(_,_):
            return .requestParameters(parameters: ["client_id" : CLIENT_ID,
                                                   "response_type": "code",
                                                   "redirect_uri" : REDIRECT_URI],
                                      encoding: URLEncoding.queryString)
        case .search(_,_):
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
//            return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .login(_,_):
            return ["Content-Type": "application/json"]
        case .search(_,_):
            return ["Content-Type" : "application/json",
                    "Authorization" : "Bearer \(globalKeychain.search(key: "access_token"))"]
        }
    }
}
