//
//  SongViewModel.swift
//  Entrenamiento
//
//  Created by Prats on 31/05/18.
//  Copyright © 2018 Carolina. All rights reserved.
//

import Foundation
import OAuthSwift

class SongViewModel {
    private static var instance: SongViewModel?
    
    // MARK: - Singleton
    static func getSharedInstance() -> SongViewModel {
        guard let sharedInstance = instance else {
            instance = SongViewModel()
            return instance!
        }
        return sharedInstance
    }
    
    // MARK: - Initializers
    
    
    // MARK: - Targets
    func getSongs(of type: String, matching: String, response: @escaping (_ song: [Song]?, _ error: String?) -> Void) {
        Song.getSongs(of: type, matching: matching) { (songs, error) in
            if let songs = songs {
                response(songs, nil)
            } else {
                response(nil, error)
            }
        }
    }
}
